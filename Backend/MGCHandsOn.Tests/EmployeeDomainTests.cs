using MGCHandsOn.Domain;
using MGCHandsOn.Domain.Repository;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace MGCHandsOn.Tests
{
    public class EmployeeDomainTests
    {
        [Fact]
        public async void GetMonthlySalaryEmployee_WhenIdExists_AnnualSalaryIsCalculatedCorrectly()
        {
            //Arrange
            var employeeRepository = new Mock<IEmployeeRepository>();
            var employeeFactory = new EmployeeFactory();
            var employeeDomain = new EmployeeDomain(employeeRepository.Object, employeeFactory);
            //Act
            employeeRepository.Setup(e => e.GetEmployeesAsync(null).Result).Returns(fakeMonthlyEmployee);
            var employee = await employeeDomain.GetEmployees(null);
            Assert.Equal(employee.First().AnnualSalary, fakeMonthlyEmployee.First().MonthlySalary * 12);
            Assert.Equal(employee.First().Id, fakeMonthlyEmployee.First().Id);
        }

        [Fact]
        public async void GetHourlySalaryEmployee_WhenIdExists_AnnualSalaryIsCalculatedCorrectly()
        {
            //Arrange
            var employeeRepository = new Mock<IEmployeeRepository>();
            var employeeFactory = new EmployeeFactory();
            var employeeDomain = new EmployeeDomain(employeeRepository.Object, employeeFactory);
            //Act
            employeeRepository.Setup(e => e.GetEmployeesAsync(null).Result).Returns(fakeHourlyEmployee);
            var employee = await employeeDomain.GetEmployees(null);
            Assert.Equal(employee.First().AnnualSalary, 120 * fakeHourlyEmployee.First().HourlySalary * 12);
            Assert.Equal(employee.First().Id, fakeHourlyEmployee.First().Id);
        }

        private readonly List<Employee> fakeMonthlyEmployee = new List<Employee>() { new Employee
        {
            Id = 1,
            Name = "Nahuel",
            ContractTypeName = "MonthlySalaryEmployee",
            RoleId = 1,
            RoleDescription = "Role",
            MonthlySalary = 3500,
            HourlySalary = 35
        } };

        private readonly List<Employee> fakeHourlyEmployee = new List<Employee>() { new Employee
        {
            Id = 1,
            Name = "Nahuel",
            ContractTypeName = "HourlySalaryEmployee",
            RoleId = 1,
            RoleDescription = "Role",
            MonthlySalary = 2000,
            HourlySalary = 15
        } };
    }
}
