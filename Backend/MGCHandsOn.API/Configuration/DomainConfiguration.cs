﻿using MGCHandsOn.Domain;
using MGCHandsOn.Domain.Repository;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MGCHandsOn.API.Configuration
{
    public static class DomainConfiguration
    {
        public static IServiceCollection AddDomainConfiguration(this IServiceCollection services)
        {
            return services
                .AddScoped<IEmployeeFactory, EmployeeFactory>()
                .AddScoped<IEmployeeDomain, EmployeeDomain>()
                .AddScoped<IEmployeeRepository, EmployeeRepository>();
        }
    }
}
