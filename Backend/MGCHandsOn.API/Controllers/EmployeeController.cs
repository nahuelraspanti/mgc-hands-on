﻿using MGCHandsOn.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MGCHandsOn.API.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class EmployeeController : ControllerBase
    {
        private readonly ILogger<EmployeeController> _logger;
        private readonly IEmployeeDomain _employeeDomain;

        public EmployeeController(ILogger<EmployeeController> logger, IEmployeeDomain employeeDomain)
        {
            _logger = logger;
            _employeeDomain = employeeDomain;
        }

        [HttpGet]
        [Route("GetEmployees")]
        public async Task<IEnumerable<Employee>> GetEmployees(int? employeeId)
        {
            var employees = await _employeeDomain.GetEmployees(employeeId);
            return employees;
        }
    }
}
