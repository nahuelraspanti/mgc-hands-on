﻿using MGCHandsOn.Domain.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MGCHandsOn.Domain
{
    public class EmployeeDomain : IEmployeeDomain
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IEmployeeFactory _employeeFactory;

        public EmployeeDomain(IEmployeeRepository employeeRepository, IEmployeeFactory employeeFactory)
        {
            _employeeRepository = employeeRepository;
            _employeeFactory = employeeFactory;
        }

        public async Task<IEnumerable<Employee>> GetEmployees(int? employeeId)
        {
            var employees = await _employeeRepository.GetEmployeesAsync(employeeId);
            var employeesToReturn = new List<Employee>();
            foreach (var item in employees)
            {
                employeesToReturn.Add(_employeeFactory.CreateEmployee(item));
            }
            return employeesToReturn;
        }
    }
}
