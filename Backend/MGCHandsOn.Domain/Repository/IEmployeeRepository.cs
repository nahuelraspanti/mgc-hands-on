﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MGCHandsOn.Domain.Repository
{
    public interface IEmployeeRepository
    {
        Task<IEnumerable<Employee>> GetEmployeesAsync(int? employeeId);
    }
}
