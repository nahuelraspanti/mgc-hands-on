﻿using MGCHandsOn.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MGCHandsOn.Domain.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly IEmployeeAPI _employeeAPI;
        public EmployeeRepository(IEmployeeAPI employeeAPI)
        {
            _employeeAPI = employeeAPI;
        }

        public async Task<IEnumerable<Employee>> GetEmployeesAsync(int? employeeId)
        {
            var allEmployees = await _employeeAPI.GetAsync<IEnumerable<Employee>>("");
            if (employeeId.HasValue)
            {
                return allEmployees.Where(e => e.Id == employeeId);
            }
            return allEmployees;
        }



    }
}
