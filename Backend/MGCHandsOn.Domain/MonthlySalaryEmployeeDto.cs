﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MGCHandsOn.Domain
{
    public class MonthlySalaryEmployeeDto : Employee
    {
        public override double AnnualSalary { get => base.AnnualSalary = MonthlySalary * 12; }
    }
}
