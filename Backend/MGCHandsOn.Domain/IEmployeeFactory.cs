﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MGCHandsOn.Domain
{
    public interface IEmployeeFactory
    {
        Employee CreateEmployee(Employee employee);
    }
}
