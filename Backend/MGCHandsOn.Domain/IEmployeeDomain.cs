﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MGCHandsOn.Domain
{
    public interface IEmployeeDomain
    {
        Task<IEnumerable<Employee>> GetEmployees(int? employeeId);
    }
}
