﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MGCHandsOn.Domain
{
    public class EmployeeFactory : IEmployeeFactory
    {
        public EmployeeFactory()
        {

        }

        public Employee CreateEmployee(Employee employee)
        {
            switch (employee.ContractTypeName)
            {
                case EmployeeContractType.HourlySalaryEmployee:
                    return CreateHourlySalaryEmployee(employee);
                case EmployeeContractType.MonthlySalaryEmployee:
                    return CreateMonthlySalaryEmployee(employee);
                default:
                    throw new Exception("Invalid contract Type!");
            }
        }

        private HourlySalaryEmployeeDto CreateHourlySalaryEmployee(Employee employee)
        {
            return new HourlySalaryEmployeeDto
            {
                ContractTypeName = employee.ContractTypeName,
                HourlySalary = employee.HourlySalary,
                Id = employee.Id,
                RoleDescription = employee.RoleDescription,
                Name = employee.Name,
                MonthlySalary = employee.MonthlySalary,
                RoleId = employee.RoleId,
                RoleName = employee.RoleName
            };
        }

        private MonthlySalaryEmployeeDto CreateMonthlySalaryEmployee(Employee employee)
        {
            return new MonthlySalaryEmployeeDto
            {
                ContractTypeName = employee.ContractTypeName,
                HourlySalary = employee.HourlySalary,
                Id = employee.Id,
                RoleDescription = employee.RoleDescription,
                Name = employee.Name,
                MonthlySalary = employee.MonthlySalary,
                RoleId = employee.RoleId,
                RoleName = employee.RoleName
            };
        }

    }
}
