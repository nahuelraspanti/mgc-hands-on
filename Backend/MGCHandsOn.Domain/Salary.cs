﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MGCHandsOn.Domain
{
    public abstract class Salary
    {
        public double HourlySalary { get; set; }

        public double MonthlySalary { get; set; }

        public virtual double AnnualSalary { get; protected set; }
    }
}
