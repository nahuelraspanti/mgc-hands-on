﻿namespace MGCHandsOn.Domain
{
    public class EmployeeContractType
    {
        public const string HourlySalaryEmployee = "HourlySalaryEmployee";
        public const string MonthlySalaryEmployee = "MonthlySalaryEmployee";
    }
}
