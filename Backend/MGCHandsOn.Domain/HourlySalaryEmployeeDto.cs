﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MGCHandsOn.Domain
{
    public class HourlySalaryEmployeeDto : Employee
    {
        public override double AnnualSalary { get => base.AnnualSalary = 120 * HourlySalary * 12; }
    }
}
