﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MGCHandsOn.Infrastructure
{
    public interface IBaseAPI
    {
        Task<T> GetAsync<T>(string url);
    }
}
