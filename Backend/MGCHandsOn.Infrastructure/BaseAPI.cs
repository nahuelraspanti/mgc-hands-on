﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace MGCHandsOn.Infrastructure
{
    public class BaseAPI : IBaseAPI
    {
        private readonly HttpClient _httpClient;

        public BaseAPI(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<T> GetAsync<T>(string url)
        {
            var content = await _httpClient.GetAsync(url);
            return (await ProcessResponse<T>(content));
        }

        private async Task<T> ProcessResponse<T>(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                await ProcessResponse(response);
            }
            string responseBody = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(responseBody);
        }

        private async Task ProcessResponse(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode) return;
            string responseBody = await response.Content.ReadAsStringAsync();

            var error = JsonConvert.DeserializeObject(responseBody);

            throw new HttpRequestException();
        }
    }
}
