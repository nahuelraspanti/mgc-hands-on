﻿using System.Net.Http;

namespace MGCHandsOn.Infrastructure
{
    public class EmployeeAPI : BaseAPI, IEmployeeAPI
    {
        private readonly HttpClient _httpClient;

        public EmployeeAPI(HttpClient httpClient) : base(httpClient)
        {
            _httpClient = httpClient;
        }
    }
}
