import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-employee-button',
  templateUrl: './employee-button.component.html',
  styleUrls: ['./employee-button.component.scss']
})
export class EmployeeButtonComponent  {
  @Output() onGetEmployees = new EventEmitter<null>();
  constructor() { }

  onClick() {
    this.onGetEmployees.emit(null);
  }

}
