import { Component, Input, OnInit } from '@angular/core';
import { Employee } from 'src/app/services/employee.service';

@Component({
  selector: 'app-employee-table',
  templateUrl: './employee-table.component.html',
  styleUrls: ['./employee-table.component.scss']
})
export class EmployeeTableComponent {
  @Input() loading: boolean = false;
  @Input() employees: Employee[] = [];

  constructor() { }

}
