import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-employee-field',
  templateUrl: './employee-field.component.html',
  styleUrls: ['./employee-field.component.scss']
})
export class EmployeeFieldComponent {
  @Output() employeeId = new EventEmitter<string>();

  constructor() { }

  onInput(event: InputEvent){
    this.employeeId.emit(event.data);
  }

}
