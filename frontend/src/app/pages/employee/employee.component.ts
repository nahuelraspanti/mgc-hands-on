import { Component, OnInit } from '@angular/core';
import { Employee, EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent {
  public employees: Employee[] = [];  
  public employeeId: string = '';
  public loading: boolean = false;

  constructor(private employeeService: EmployeeService) { }

  onGetEmployees(): void{
    const id = this.employeeId.trim() ? this.employeeId : '';
    this.loading = true;
    this.employeeService.getEmployees(id).subscribe( (employees) => {
      this.employees = employees;
      this.loading = false;
    })
  }

}
