import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeRoutingModule } from './employee-routing.module';
import { EmployeeTableComponent } from './employee-table/employee-table.component';
import { EmployeeButtonComponent } from './employee-button/employee-button.component';
import { EmployeeComponent } from './employee.component';
import { EmployeeFieldComponent } from './employee-field/employee-field.component';


@NgModule({
  declarations: [EmployeeTableComponent, EmployeeButtonComponent, EmployeeComponent, EmployeeFieldComponent],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
  ]
})
export class EmployeeModule { }
