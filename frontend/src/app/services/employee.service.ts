import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

export interface Employee{
  id: number;
  name: string;
  contractTypeName: string;
  roleId: number;
  roleName: string;
  roleDescription: null | string;
  hourlySalary: number;
  monthlySalary: number;
  annualSalary: number;
}

const API_URL = 'http://localhost:5000/api';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  getEmployees(id?: string): Observable<Employee[]> {
    return this.http.get<Employee[]>(`${API_URL}/employee/getemployees?employeeId=${id}`)
      .pipe(
        catchError((err: HttpErrorResponse) => {
          console.error(err.message);
          return [];
        })
      );
  }
}
